package forms

import javax.inject.Singleton

import play.api.data._
import play.api.data.Forms._
import services.{Department, Employee}

@Singleton
class EmployeeFormFactory {

  def employee(): Form[EmployeeData] = {
    Form(
      mapping(
        "name" -> nonEmptyText,
        "salary" -> number(1, 200, false),
        "department" -> nonEmptyText
      )(EmployeeData.apply)(EmployeeData.unapply)
    )
  }
}

case class EmployeeData(name: String, salary: Int, department: String) {
  def toEmployee = Employee(name, salary, Department(department))
}