package controllers

import javax.inject._

import forms.{EmployeeData, EmployeeFormFactory}
import play.api.Logger
import play.api.data.Form
import play.api.mvc._
import services.{DbLimitExceeded, EmployeeDB, SalaryCounter}

import scala.util.{Failure, Success, Try}

@Singleton
class EmployeeController @Inject() (
                                    employeeDB: EmployeeDB,
                                    employeeForm: EmployeeFormFactory,
                                    salaryCounter: SalaryCounter
                                  ) extends Controller {

  def index = Action {
    Ok(views.html.index(
      "Your new application is ready.",
      employeeDB.employees,
      employeeDB.departments,
      formEmployee,
      salaryCounter
    ))
  }

  def addEmployee = Action { implicit request =>
    formEmployee.bindFromRequest.fold(
      formWithErrors => Ok(views.html.addEmployee(formWithErrors)),
      saveEmployeeData(request, _)
    )
  }

  def saveEmployeeData(implicit request: Request[_], employeeData: EmployeeData) = {
    Try {
      employeeDB.addEmployee(employeeData.toEmployee)
    } match {
      case Success(_) => Redirect(routes.EmployeeController.index)
      case Failure(exc: DbLimitExceeded) => {
        Ok(views.html.addEmployee(formEmployee.bindFromRequest().withGlobalError(exc.getMessage)))
      }
      case Failure(anyExc) => throw anyExc
    }
  }

  def formEmployee = employeeForm.employee()
}
