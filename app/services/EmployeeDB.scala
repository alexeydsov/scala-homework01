package services

import javax.inject.{Inject, Named, Singleton}

import play.api.Logger

trait EmployeeDB {
  def departments: List[Department]
  def employees: List[Employee]

  def addDepartment(department: Department): Department
  def addEmployee(employee: Employee): Employee
}

case class LimitsConfig(employeesLimit: Option[Int], departmentsLimit: Option[Int])

@Singleton
class EmployeeDBInstance @Inject() (val limits: LimitsConfig) extends EmployeeDB{
  Logger.info("CONFIG:      " + limits.toString)

  var departments = List[Department]()
  var employees = List[Employee]()

  override def addDepartment(department: Department): Department = {
    assertLimit(departments, limits.departmentsLimit, "departments limit reached")

    departments :+= department
    department
  }

  override def addEmployee(employee: Employee): Employee = {
    assertLimit(employees, limits.employeesLimit, "employees limit reached")

    if (departments.filter(_ == employee.department).isEmpty) {
      addDepartment(employee.department)
    }

    employees :+= employee
    employee
  }

  private def assertLimit(list: Seq[_], limit: Option[Int], message: String) = {
    if (limit.isDefined && list.size >= limit.get) {
      throw new DbLimitExceeded(message)
    }
  }
}

case class Employee(name: String, salary: Int, department: Department)

case class Department(name: String)

class DbLimitExceeded(message: String) extends Exception(message)