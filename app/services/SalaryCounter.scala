package services

import javax.inject.{Inject, Singleton}

trait SalaryCounter {

  def avgByDepartments: Map[Department, Int]

  def maxByDepartments: Map[Department, Int]
}

@Singleton
class SalaryCounterInstance @Inject() (employeeDB: EmployeeDB) extends SalaryCounter {

  override def avgByDepartments = {
    departmentCountSum.mapValues(accum => accum._2 / accum._1)
  }

  override def maxByDepartments = {
    departmentGroupped
      .mapValues(
        _.foldLeft(0)((max, employee) => Math.max(max, employee.salary))
      )
  }

  private def departmentCountSum = {
    departmentGroupped
      .mapValues(
        _.foldLeft((0, 0))
        ((accum, employee) => (accum._1 + 1, accum._2 + employee.salary))
      )
  }

  private def departmentGroupped = employeeDB.employees.groupBy(_.department)
}
